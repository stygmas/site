#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Stygmas'
SITENAME = 'Stygmas'
#SITEURL = 'stygmas.band'

PATH = 'content'

TIMEZONE = 'America/Maceio'

DEFAULT_LANG = 'br'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Links', '#'),)

# Social widget
SOCIAL = (('Twiter', '#'), ('Youtube', '#'),)

#Pages
#Pages
DIRECT_TEMPLATES = ('news', 'media', 'store', '404', 'index')
DISPLAY_PAGES_ON_MENU  = True
NEWS_URL = 'news/'
NEWS_SAVE_AS = 'news/index.html'
MEDIA_URL = 'media/'
MEDIA_SAVE_AS = 'media/index.html'
STORE_URL = 'store/'
STORE_SAVE_AS = 'store/index.html'
MENUITEMS = (('Novidades', 'news/'), ('Mídia', 'media/'), ('Loja', 'store/'))

DEFAULT_PAGINATION = 8

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'storm'
