title: Stygmas
date: 2020-4-4
category: Stygmas

Stygmas é um power trio de Rock com influências de Grunge, Groove, Hardcore e Heavy Metal.

Stygmas é formada por:

## Daniel Pimentel (Guitarra e Voz)

<img src="/theme/img/daniel.jpg" alt="Guitarra e Voz" style="height: 320px; width:240px;"/>

## Erick Jhanson (Baixo e Voz)

<img src="/theme/img/erick.jpeg" alt="Baixo e Voz" style="height: 320px; width:240px;"/>

## Tiago Ventura (Bateria e Voz).

<img src="/theme/img/tiago.jpeg" alt="Bateria e Voz" style="height: 320px; width:240px;"/>

